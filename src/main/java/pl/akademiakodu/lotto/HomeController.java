package pl.akademiakodu.lotto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class HomeController {


    @GetMapping("")
    public String generatorLotto(ModelMap map) {
        LottoGenerator lottoGenerator = new LottoGenerator();
        map.put("numbers", lottoGenerator.generate());
        return "lotto";
    }


}
